![Socialissues logo](img/logo.png)

<!-- .element: class="logo" -->

# socialissues.tech

## sobtec 2020

---

## Glossari

FLOSS / Software lliure

<!-- .element: class="fragment fade-in-then-semi-out" -->

Repositori git

<!-- .element: class="fragment fade-in-then-semi-out" -->

Issue

<!-- .element: class="fragment fade-in-then-semi-out" -->

---

## Què

Socialissues.tech és un agregador d'issues de projectes de l'Economia Social i Solidària.

---

## Per què

---

<!-- .slide: data-background="img/hacktoberfest_bg.png" -->

~ **50.000** participants

<!-- .element: class="text-dark-bg fragment fade-in" -->

\> **250.000** issues resoltes

<!-- .element: class="text-dark-bg fragment fade-in" -->

\> **250** trobades en **50** països

<!-- .element: class="text-dark-bg fragment fade-in" -->

---

<!-- .slide: data-background="img/first_contrib_bg.png" -->

Note:
First Contrib
Projectes FLOSS categoritzats amb etiquetes

---

<!-- .slide: data-background="img/issuehub_bg.png" -->

Note:
Issuehub
Filtra directament les issues

---

<!-- .slide: data-background="img/codetriage_bg.png" -->

Note:
Codetriage
Filtra per lang, t'apuntes a projectes i envia mail diari amb issues

---

<!-- .slide: data-background="img/ovio_bg.png" -->

Note:
Online Volunteering in the Open
Sustainable Development Goals de Nacions Unides

---

## Per què?

Comunitat FLOSS ❤️ Ecosistema ESS

<!-- .element: class="fragment fade-in-then-semi-out" -->

Visibilitzar projectes ESS

<!-- .element: class="fragment fade-in-then-semi-out" -->

Sinergies entre projectes

<!-- .element: class="fragment fade-in-then-semi-out" -->

Note:

- Per visibilitzar els projectes fora de l'ESS.
- Perquè els projectes avancin i tinguin més contribuïdores.
- Per impulsar sinergies entre els projectes.
- Per posar en valor l'organització d'un projecte FLOSS acollidor.

---

## Com a contribuïdora

Issues amb impacte social

<!-- .element: class="fragment fade-in-then-semi-out" -->

Visió tècnica a projectes ESS

<!-- .element: class="fragment fade-in-then-semi-out" -->

Note:

- T'obre portes a un conjunt d'issues amb impacte directe sobre la societat.
- Pots aportar solucions a les diferents dificultats que poden tenir les entitats amb els teus coneixements tecnològics. Així com donar una visió tècnica global a aquests projectes.

---

## Com a entitat/projecte

Visibilitzar el vostre projecte més enllà de l'ESS

<!-- .element: class="fragment fade-in-then-semi-out" -->

Solucions enriquides amb visions diverses

<!-- .element: class="fragment fade-in-then-semi-out" -->

Empenta al vostre projecte

<!-- .element: class="fragment fade-in-then-semi-out" -->

Note:

- Un espai on visibilitzar el vostre projecte a persones que, per altres mitjans, no arribarien a conèixer la iniciativa.
- Solucions a les vostres issues des d'una visió externa a l'ESS, situació que no es dóna molt sovint.
- Oportunitats per endreçar els vostres projectes i documentar-los per tal d'obrir el vostre codi a noves contribuïdores.
- Pot suposar una empenta al vostre projecte.

---

## Com es materialitza

---

<!-- .slide: data-background="img/socialissues_bg.png" -->

---

<!-- .slide: data-background="img/socialissues_detail_bg.png" -->

---

<!-- .slide: data-background="img/timeoverflow_github_bg.png" -->

---

## Qui

Coopdevs, Adabits, Dabne, Col·lectivaT, LliureTic, Jamgo

<!-- .element: class="fragment fade-in-then-semi-out" -->

Socialissues.tech, Pam a pam, Open Food Network, TimeOverflow

<!-- .element: class="fragment fade-in-then-semi-out" -->

Note:
Som una aliança entre entitats de l'ESS per ajudar-nos mútuament a evolucionar i millorar el nostre ecosistema.

---

![Coopdevs logo](img/coopdevs.svg)

<!-- .element: class="logo" -->

---

Més informació a [socialissues.tech](socialissues.tech)
